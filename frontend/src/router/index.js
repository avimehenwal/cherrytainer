import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import ProcessDataTable from '@/views/ProcessDataTable.vue'
import Monitor from '@/views/Monitor.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home,
    icon: 'mdi-home'
  },
  {
    path: '/about',
    name: 'About',
    icon: 'mdi-help-box',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  },
  {
    path: '/process',
    name: 'Process',
    component: ProcessDataTable,
    icon: 'mdi-table'
  },
  {
    path: '/monitor',
    name: 'Monitor',
    component: Monitor,
    icon: 'mdi-ballot'
  }

]

const router = new VueRouter({
  routes
})

export default router
