import cherrypy
import cherrypy_cors
from cherrytainer import CherryTainer
from api_monitor import MonitorAPI
# from api_docker import DockerAPI


@cherrypy.tools.json_out()
class AppServer():

    def __init__(self):
        self.api = CherryTainer()

    @cherrypy.expose
    def index(self):
        """ Dashboard Info counters ONLY """
        # data["version"]      = self.api.version()
        # data["containers"]   = self.api.list_all_containers()
        # data["system_info"]  = self.api.system_info()
        # data['process_list'] = self.api.process_list()
        return self.api.get_overview()

    @cherrypy.expose
    def process(self):
        """ Todo: add count ? """
        return self.api.get_processes()

    @cherrypy.expose
    def cpu(self):
        return self.api.get_cpu()

    @cherrypy.expose
    def memory(self):
        return self.api.get_memory()

    @cherrypy.expose
    def disk(self):
        return self.api.get_disk()

    @cherrypy.expose
    def network(self):
        return self.api.get_network()

    @cherrypy.expose
    def sensor(self):
        return self.api.get_sensor()

    @cherrypy.expose
    def users(self):
        return self.api.get_users()

    @cherrypy.expose
    def docker(self):
        pass



# @cherrypy.tools.json_in()

# @cherrypy.popargs('primary')
class MonitorApp():

    @cherrypy.expose
    @cherrypy.tools.json_out()
    def index(self):
        api = MonitorAPI()
        # cherrypy.log(path)
        result = dict()
        result['boot_time'] = api.boot_time()
        result['users']     = api.users()
        # result['path']      = primary
        return result



# cherrypy.server.ssl_module = 'builtin'
# cherrypy.server.ssl_certificate = "cert.pem"
# cherrypy.server.ssl_private_key = "privkey.pem"

cherrypy_cors.install()
cherrypy.config.update({
    'server.socket_host': '127.0.0.1',
    'cors.expose.on': True,
    'server.socket_port': 9090,
})

# Add Routing
monitor_api_conf = dict()
# docker_api_conf  = dict()

# method -> uri path -> config
cherrypy.tree.mount(MonitorApp(), '/mon', monitor_api_conf)
# cherrypy.tree.mount(DockerAPI(), '/docker', docker_api_conf)

cherrypy.quickstart(AppServer())