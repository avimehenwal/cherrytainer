import config
import psutil

""" python system and process utilities
    [ ] cross platform
    [ ] cpu utilization
    [ ] virtual memory utilization
    [ ] disk space
    [ ] network
    [ ] sensors
    [ ] processes

ps, top, lsof, netstat, ifconfig, who,df, kill, free, nice,
ionice, iostat, iotop, uptime, pidof, tty, taskset, pmap

    /cpu/time
    /cpu/time/percent           breakdown at user, system, idle level
    /cpu/percent
    /cpu/percent/5
    /cpu/count
    /cpu/stats
    /cpu/frequency
    /cpu/load                   average system load over the last 1, 5 and 15 minutes as a tuple

    /mem/               total, available - expressed in bytes
    /mem/swap

    /disk
    /disk/usage         device name, mountpoint, fstype, options r/w

    /network/
    /network/counters
    /network/connections
    /network/address
    /network/stats

    /sensor/
    /sensor/temperature         it may be a CPU, an hard disk or something else, depending on the OS and its configuration)
    /sensor/fans
    /sensor/battery
    /sensor/

    /boottime                   in sec since epoch
    /users                      users currently connected on the system

    /process/
    /process/pids
    /process/all                process_iter
    /process/:id                show children
    /process/:id/kill
    /process/


user   : time spent by normal processes executing in user mode;
            on Linux this also includes guest time
system : time spent by processes executing in kernel mode
idle   : time spent doing nothing

[ ] For individual CPU or aggregated?

    total: total physical memory (exclusive swap).
available: the memory that can be given instantly to processes without the system going into swap. This is calculated by summing different memory values depending on the platform and it is supposed to be used to monitor actual memory usage in a cross platform fashion.
Other metrics:

used: memory used, calculated differently depending on the platform and designed for informational purposes only. total - free does not necessarily match used.
free: memory not being used at all (zeroed) that is readily available; note that this doesn’t reflect the actual memory available (use available instead). total - used does not necessarily match free.
active (UNIX): memory currently in use or very recently used, and so it is in RAM.
inactive (UNIX): memory that is marked as not used.
buffers (Linux, BSD): cache for things like file system metadata.
cached (Linux, BSD): cache for various things.
shared (Linux, BSD): memory that may be simultaneously accessed by multiple processes.
slab (Linux): in-kernel data structures cache.
wired (BSD, macOS): memory that is marked to always stay in RAM. It is never moved to disk.


    bytes_sent: number of bytes sent
bytes_recv: number of bytes received
packets_sent: number of packets sent
packets_recv: number of packets received
errin: total number of errors while receiving
errout: total number of errors while sending
dropin: total number of incoming packets which were dropped
dropout: total number of outgoing packets which were dropped (always 0 on macOS and BSD)

"""

class MonitorAPI():

    def __init__(self):
        """ Add local API related metadata """
        self.__api_name      = config.API_NAME
        self.__api_version   = config.API_VERSION
        self.__api_developer = config.API_DEVELOPER
        self.__api_app       = config.API_APP

    def boot_time(self):
        return psutil.boot_time()

    def users(self):
        return psutil.users()




