import cherrypy

"""
# TEST
curl -L http://127.0.0.1:8080/sdfds/sdfdsf
{"Primary": "sdfds", "Secondary": "sdfdsf"}
"""

# @cherrypy.popargs('name')
# class Band(object):
#     def __init__(self):
#         self.albums = Album()

#     @cherrypy.expose
#     def index(self, name):
#         return 'About %s...' % name

# @cherrypy.popargs('title')
# class Album(object):
#     @cherrypy.expose
#     def index(self, name, title):
#         return 'About %s by %s...' % (title, name)

#  accepts 2 path
@cherrypy.popargs('primary', 'secondary')
class App(object):

    """ query string as exposed method args
    query string -> @cherrypy.popargs('title')
    """

    @cherrypy.expose
    @cherrypy.tools.json_out()
    def index(self, primary, secondary):
        # print(dir(cherrypy.request.body))
        # print(dir(cherrypy.request.name))
        # print(dir(cherrypy.request.params.keys))
        # print(dir(cherrypy.request.read))
        # print(name)
        return {'Primary' : primary, 'Secondary': secondary }

if __name__ == '__main__':
    # cherrypy.quickstart(Band())
    cherrypy.quickstart(App())
