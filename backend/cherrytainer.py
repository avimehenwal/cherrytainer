import docker
import pdb
from pprint import pprint
import platform
import socket
import re
import uuid
import json
import psutil

"""
[] Add last_updated time?
"""

class CherryTainer():

    def __init__(self):
        self.docker = docker.from_env()

    def list_all_containers(self):
        # return [container.id for container in self.docker.containers.list(all=True)]
        result_list = list()
        for item in self.docker.containers.list(all=True):
            container_info = dict()
            container_info['status']   = item.status
            container_info['name']     = item.name
            container_info['short_id'] = item.short_id
            # print(container_info)
            result_list.append(container_info)
        # pdb.set_trace()
        return result_list

    def run_container(self):
        print(self.docker.containers.run("alpine", ["echo", "hello", "world"]))

    def version(self):
        r = self.docker.version()
        print(type(r))
        return r

    def docker_stat(self):
        return {
            'active': len(self.docker.containers.list()),
            'total' : len(self.docker.containers.list(all=True))
        }

    def system_info(self):
        info=dict()
        info['platform']         = platform.system()
        info['platform-release'] = platform.release()
        info['platform-version'] = platform.version()
        info['architecture']     = platform.machine()
        info['hostname']         = socket.gethostname()
        info['ip-address']       = socket.gethostbyname(socket.gethostname())
        info['mac-address']      = ':'.join(re.findall('..', '%012x' % uuid.getnode()))
        info['processor']        = psutil.cpu_count()   # platform.processor()
        info['ram']=str(round(psutil.virtual_memory().total / (1024.0 **3)))+" GB"
        info['process_count']    = len(psutil.pids())
        return info

    def get_processes(self):
        listOfProcessNames = list()
        for proc in psutil.process_iter():
            pInfoDict = proc.as_dict(attrs=['pid', 'name', 'cpu_percent',
             'memory_percent', 'cpu_times', 'create_time', 'memory_info'])
            listOfProcessNames.append(pInfoDict)
        return listOfProcessNames

    def get_cpu(self):
        result = dict()
        result['cpu_times']         = psutil.cpu_times()
        result['cpu_percent']       = psutil.cpu_percent()
        result['cpu_times_percent'] = psutil.cpu_times_percent()
        result['cpu_count']         = psutil.cpu_count()
        result['cpu_stats']         = psutil.cpu_stats()
        result['cpu_freq']          = psutil.cpu_freq()
        result['getloadavg']        = psutil.getloadavg()
        return result

    def get_memory(self):
        result = dict()
        result['virtual_memory'] = psutil.virtual_memory()
        result['swap_memory']    = psutil.swap_memory()
        return result

    def get_disk(self):
        result = dict()
        result['disk_partitions']       = psutil.disk_partitions()
        result['disk_io_counters']      = psutil.disk_io_counters()
        result['disk_usage']            = dict()
        result['disk_usage']['total']   = self.bytes2human(psutil.disk_usage('/').total)
        result['disk_usage']['used']    = self.bytes2human(psutil.disk_usage('/').used)
        result['disk_usage']['free']    = self.bytes2human(psutil.disk_usage('/').free)
        result['disk_usage']['percent'] = psutil.disk_usage('/').percent
        return result

    def get_network(self):
        result = dict()
        result['net_io_counters'] = psutil.net_io_counters()
        result['net_connections'] = psutil.net_connections()
        result['net_if_addrs']    = psutil.net_if_addrs()
        result['net_if_stats']    = psutil.net_if_stats()
        return result

    def get_sensor(self):
        result = dict()
        result['sensors_temperatures'] = psutil.sensors_temperatures()
        result['sensors_fans']         = psutil.sensors_fans()
        result['sensors_battery']      = psutil.sensors_battery()
        result['boot_time']            = psutil.boot_time()
        result['users']                = psutil.users()
        return result

    def get_users(self):
        return psutil.users()

    def bytes2human(self, n):
        # http://code.activestate.com/recipes/578019
        # >>> bytes2human(10000)
        # '9.8K'
        # >>> bytes2human(100001221)
        # '95.4M'
        symbols = ('K', 'M', 'G', 'T', 'P', 'E', 'Z', 'Y')
        prefix = {}
        for i, s in enumerate(symbols):
            prefix[s] = 1 << (i + 1) * 10
        for s in reversed(symbols):
            if n >= prefix[s]:
                value = float(n) / prefix[s]
                return '%.1f%s' % (value, s)
        return "%sB" % n

    def get_overview(self):
        data = dict()
        data['process']            = dict()
        data['cpu']                = dict()
        data['memory']             = dict()
        data['swap']               = dict()
        data['disk']               = dict()
        data['network']            = dict()
        data['users']              = dict()
        data['battery']            = dict()
        data['docker']             = self.docker_stat()
        data['process']['count']   = len(self.get_processes())
        data['cpu']['percent']     = psutil.cpu_percent()
        data['memory']['percent']  = psutil.virtual_memory().percent
        data['swap']['percent']    = psutil.swap_memory().percent
        data['disk']['percent']    = psutil.disk_usage('/').percent
        data['network']['count']   = len(psutil.net_connections())
        data['users']['count']     = len(psutil.users())
        data['battery']['percent'] = psutil.sensors_battery().percent
        # data['battery']['percent'] = psutil.sensors_battery().percent
        # data['battery']['percent'] = psutil.sensors_battery().percent
        return data



if __name__ == '__main__':
    api = CherryTainer()
    # api.get_containers_all()

    # api.list_containers()
    # pprint(api.list_all_containers())
    pprint(api.docker_stat())
    # pprint(api.system_info())
    # pprint(api.process_list())

