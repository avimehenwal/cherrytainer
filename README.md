# cherrytainer

![](demo.png)

CherryPy + Docker API

### Why cherryPi

* [docker API has official python SDK](https://docs.docker.com/engine/api/)
* Minimilastic microframework
* Plan to contribute


### Why Vue.JS

* Learn frontend

```sh
sudo apt install -y nodejs

curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -
echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list
sudo apt update && sudo apt install yarn
curl -L https://npmjs.org/install.sh | sudo sh

npm install -g @vue/cli
sudo yarn global add @vue/cli
vue --version

vue create frontend
cd frontend
vue add vuetify
yarn add axios
yarn add vue-google-charts
yarn serve
```

VUETIFY Themes

1. https://github.com/creativetimofficial/vuetify-material-dashboard
2. https://github.com/vuetifyjs/theme-blog

### Frontend

- CTA - Call to action
- bar charts - sparklines
- [Pie charts - circular loaders](https://vuetifyjs.com/en/components/progress-circular/)

### How to make excellent dashboards

* https://uxplanet.org/10-rules-for-better-dashboard-design-ef68189d734c
* [Model View - viewModel](https://www.youtube.com/watch?v=fo6rvTP9kkc)
* [Material design visualizations considerations](https://material.io/design/communication/data-visualization.html#behavior)
* [Material Design color choice primar/secondary](https://material.io/design/color/the-color-system.html#color-theme-creation)

## Inspirations

* https://github.com/Jahaja/psdash
* https://psutil.readthedocs.io/en/latest/
* https://github.com/hubbcaps/gazee/blob/master/Gazee.py

## Steps

1. [ ] Setup environment
2. [ ] Configure vscode task
3. [ ] return JSON from backend to frontend
4. [ ] install [vue CLI `frontend`](https://cli.vuejs.org/)
5. [ ] [setup server API for vueJS to consume data](https://vuejs.org/v2/cookbook/using-axios-to-consume-apis.html)
6. [ ] [Enable SSL on cherrypi, else axios wont work](https://docs.cherrypy.org/en/latest/deploy.html#ssl-support)
7. [ ] [Set up cherrypy CORS, else axios wont work](https://stackoverflow.com/questions/57416237/how-to-set-up-cors-in-cherrypy)
8. [ ] Build up UI Dashboard and Visualizations
9. [ ] [Follow step by step development](https://github.com/iamshaunjp/vuetify-playlist)
10. [ ] Use SVG animation/visualizations

> Process - Container - Services