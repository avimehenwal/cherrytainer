import Vue from 'vue'
import VueRouter from 'vue-router'
import Settings from '@/views/Settings.vue'
import Home from '../views/Home.vue'
import ProcessDataTable from '@/views/ProcessDataTable.vue'
import Posts from '@/views/Posts.vue'
import Cpu from '@/views/Cpu.vue'

// import ProcessDataTable from '@/views/ProcessDataTable.vue';
// import Monitor from '@/views/Monitor.vue';

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home,
    icon: 'mdi-home'
  },
  {
    path: '/about',
    name: 'About',
    icon: 'mdi-help-box',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  },
  {
    path: '/settings',
    name: 'Settings',
    component: Settings,
    icon: 'mdi-cog-box'
  },
  {
    path: '/process',
    name: 'Process',
    component: ProcessDataTable,
    icon: 'mdi-table'
  },
  {
    path: '/cpu',
    name: 'Cpu',
    component: Cpu,
    icon: 'mdi-table'
  },
  {
    path: '/posts',
    name: 'Posts',
    component: Posts,
    icon: 'mdi-text-box-search'
  }

  // {
  //   path: '/monitor',
  //   name: 'Monitor',
  //   component: Monitor,
  //   icon: 'mdi-ballot'
  // }

]

const router = new VueRouter({
  routes
})

export default router
