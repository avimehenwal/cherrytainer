import Vue from 'vue'
import Vuex from 'vuex'
import Posts from '../apis/Posts'
import Metrics from '../apis/Metrics'
import axios from 'axios'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    api_url: 'http://127.0.0.1:9090',
    miniVariant: false,
    results: null,
    systemInfo: null,
    process_list: null,
    posts: null,
    loading: null,
    error: null,
    errorMessage: null,
    dashboard: null,
    cpu: null
  },
  mutations: {
    // update_api
    updateAPI (state, value) {
      state.api_url = value
    },
    STORE_DASHBOARD (state, payload) {
      state.dashboard = payload
    },
    toggleSidebar (state) {
      state.miniVariant = !state.miniVariant
    },
    ADD_RESULTS (state, value) {
      if (state.results == null) {
        state.results = value
      }
    },
    FETCH_SYSTEMINFO (state) {
      if (state.system_info == null) {
        axios.get(state.api_url)
          .then((response) => {
            state.systemInfo = response.data.system_info
            state.process_list = response.data.process_list
          })
        // .catch(error => {
        //   this.apiError = true;
        // });
      }
    },
    STORE_PROCESSES (state, payload) {
      state.process_list = payload
    },
    STORE_CPU (state, payload) {
      state.cpu = payload
    },
    FTECH_POSTS (state, payload) {
      state.posts = payload
    },
    LOADING_TRUE (state) {
      state.loading = true
    },
    LOADING_FALSE (state) {
      // Wait for at least 1 second
      state.loading = false
    },
    ERROR_TRUE (state, payload) {
      state.error = true
      state.errorMessage = payload
    },
    ERROR_FALSE (state) {
      state.error = false
      state.errorMessage = null
    }
  },
  actions: {
    updateAPI (context) {
      context.commit('updateAPI')
    },
    toggleSidebar (context) {
      context.commit('toggleSidebar')
    },
    ADD_RESULTS (context, payload) {
      context.commit('ADD_RESULTS', payload)
    },
    FETCH_SYSTEMINFO (context) {
      context.commit('FETCH_SYSTEMINFO')
    },
    FETCH_API (context) {
      context.commit('FETCH_API')
    },
    FTECH_POSTS (context) {
      context.commit('LOADING_TRUE')
      // function sleep (milliseconds) {
      //   const date = Date.now()
      //   let currentDate = null
      //   do {
      //     currentDate = Date.now()
      //   } while (currentDate - date < milliseconds)
      // }
      Posts.all()
        .then(response => {
          // sleep(2000)
          context.commit('FTECH_POSTS', response.data)
          setTimeout(() => { console.log('World!') }, 2000)
          context.commit('LOADING_FALSE')
        })
        .catch(error => {
          console.log(error)
        })
    },
    FETCH_PROCESSES (context) {
      context.commit('LOADING_TRUE')
      context.commit('ERROR_FALSE')
      // console.log('Avi')
      Metrics.get_process()
        .then(response => {
          context.commit('STORE_PROCESSES', response.data)
          context.commit('LOADING_FALSE')
        })
        .catch(error => {
          console.log(error)
          context.commit('ERROR_TRUE', error)
        })
    },
    FETCH_CPU (context) {
      context.commit('LOADING_TRUE')
      context.commit('ERROR_FALSE')
      Metrics.get_cpu()
        .then(response => {
          context.commit('STORE_CPU', response.data)
          context.commit('LOADING_FALSE')
        })
        .catch(error => {
          console.log(error)
          context.commit('ERROR_TRUE', error)
        })
    },
    FETCH_DASHBOARD (context) {
      context.commit('LOADING_TRUE')
      context.commit('ERROR_FALSE')
      axios.get(context.getters.getAPI)
        .then(response => {
          console.log(context.getters.getAPI)
          context.commit('STORE_DASHBOARD', response.data)
          context.commit('LOADING_FALSE')
        })
        .catch(error => {
          console.log(error)
          context.commit('ERROR_TRUE', error)
        })
    }
  },
  modules: {
  },
  getters: {
    getAPI: state => {
      return state.api_url
    },
    getProcessCount: state => {
      return state.process_list.length
    },
    getDashboard: state => {
      return state.dashboard
    }
  }
})
