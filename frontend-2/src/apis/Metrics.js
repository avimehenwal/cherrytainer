import Apis from './Apis'

const PROCESS = 'process'

export default {
  get_process () {
    return Apis.get(PROCESS)
  },
  get_cpu () {
    return Apis.get('cpu')
  }

}
