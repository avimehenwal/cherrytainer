import Api from './Api'

const END_POINT = 'posts'

export default {
  all () {
    // will return a promise
    return Api.get(END_POINT)
  }
}
